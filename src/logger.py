"""Runner for the mfi logger utility.

If you only care about running this as a command, this is what you use.

Run '<cmd> -h' for help
"""
import time
import argparse
from mfilogger import MFiLogger

def _get_data(logger):
    """Gets the first sensor's data"""
    data = logger.get_message()["sensors"][0]
    data["time"] = int(time.time())
    return data

def fv_formatter(logger, delimiter):
    """Formats the streaming data with a field delimiter"""
    fields = ("output", "power", "current", "voltage", "powerfactor", "energy")
    print(*fields, sep=delimiter)

    while True:
        data = _get_data(logger)
        print(*(data[f] for f in fields), sep=delimiter)

def json_formatter(logger):
    """Formats the streaming data as JSON"""
    while True:
        print(_get_data(logger))

def main():
    """Executes the mfi data streamer after parsing arguments
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("--host", "-H",
                        required=True,
                        help="Hostname of mFi power device")
    parser.add_argument("--username", "-u",
                        nargs="?",
                        default="ubnt",
                        help="Username for device")
    parser.add_argument("--password", "-p",
                        nargs="?",
                        default="ubnt",
                        help="Password for device")
    parser.add_argument("--format", "-f",
                        nargs="?",
                        default="json",
                        choices=('json', 'csv', 'tsv'),
                        help="Output format")
    args = parser.parse_args()

    with MFiLogger(host=args.host,
                   username=args.username,
                   password=args.password) as logger:

        if args.format == "json":
            json_formatter(logger)
        elif args.format == "csv":
            fv_formatter(logger, ",")
        elif args.format == "tsv":
            fv_formatter(logger, "\t")

if __name__ == "__main__":
    main()
