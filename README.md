mFi Power Stream
================

A utility to dump power data from a Ubiquiti mFi device to the terminal in a stream.

It'll connect to your mFi device and dump data to the console with `logger.py`.

I wanted to get this off my drive and onto the internet so making this pretty-and-releasable wasn't a priority.

Warnings
-----
Unfortunately I can't mark this library as thread-safe at all nor will I accept responsiblity if it eats your plants or house or anything. I haven't had any issues but don't offer any guarantees.

I only ran this on Python 3.x. YMMV if you try 2.x.

Installation
------------

I'd reccomend setting up a [Virtual Environment](https://docs.python-guide.org/dev/virtualenvs/).

Install requirements: `pip install -r requirements.txt`

Execute (assuming python goes to a Python3.x install) : `python logger.py`

Usage
-----

```
usage: logger.py [-h] --host HOST [--username [USERNAME]]
                 [--password [PASSWORD]] [--format [{json,csv,tsv}]]

Persistant logger of power data from a mFi device

optional arguments:
  -h, --help            show this help message and exit
  --host HOST, -H HOST  Hostname of mFi power device
  --username [USERNAME], -u [USERNAME]
                        Username for device
  --password [PASSWORD], -p [PASSWORD]
                        Password for device
  --format [{json,csv,tsv}], -f [{json,csv,tsv}]
                        Output format
```

