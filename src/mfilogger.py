"""A helper library to help with connecting to mFi power devices.
Since the mPower line seems to have been discontinued, you can
use this to help feed data into a myriad of data sources to harvest
real-time power data.
"""

import ssl
import json

import requests
import websocket

class MFiLogger:
    """Provides a connection to a mFi device to retrieve power data

    Can be used with devices like:
        * mPower mini
        * mPower

    Should be usable by:
        * mFi-LD
        * mFi-MPW

    https://www.ubnt.com/mfi/mpower/
    """

    _LOGIN_URL_TPL = "https://{}/login.cgi"
    _WS_URL_TPL = "wss://{}:7682/?c={}"
    _WS_PROTOS = ["mfi-protocol"]
    _WS_SSL_OPTS = {"check_hostname": False, "cert_reqs": ssl.CERT_NONE}

    def __init__(self, host, username="ubnt", password="ubnt"):
        self._host = host
        self._username = username
        self._password = password
        self._token = None
        self._ws = None

    def __enter__(self):
        self.connect()
        return self

    def __exit__(self, *args):
        self._ws.close()

    def __get_token(self):
        with requests.Session() as session:
            login_url = self._LOGIN_URL_TPL.format(self._host)
            resp = session.get(login_url, verify=False)

            request_payload = dict(url="/",
                                   Submit="Login",
                                   username=self._username,
                                   password=self._password)

            login_resp = session.post(login_url,
                                      data=request_payload,
                                      cookies=resp.cookies,
                                      allow_redirects=False,
                                      verify=False)

            if login_resp.status_code not in (200, 302):
                raise Exception("Probably a bad username/password", resp.status_code)

            return resp.cookies["AIROS_SESSIONID"]

    def connect(self):
        """Establishes a connection to the mFi device to stream data
        """
        if self._token is None:
            self._token = self.__get_token()

        ws_url = self._WS_URL_TPL.format(self._host, self._token)
        self._ws = websocket.create_connection(ws_url,
                                               subprotocols=self._WS_PROTOS,
                                               sslopt=self._WS_SSL_OPTS,
                                               suppress_origin=False)

    def get_message(self):
        """Parses the message coming from the mFi and returns a Python dict
        """
        return json.loads(self._ws.recv())
